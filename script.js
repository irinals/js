// Задание 1.1 Квадраты чисел от 10 до 20 и Задание 1.2 Сумма всех чисел от 10 до 20
var sum = 0; 
for(var i = 10; i <= 20; i++){
	console.log(i * i);
	sum += i;
}
console.log(sum);

// Задание 2
function runClick(){
	var x1 = parseInt(document.getElementById('x1').value);
	var x2 = parseInt(document.getElementById('x2').value);
// Задание 2.1 После каждого нажатия кнопки Run поле вывода результатов очищается от предыдущих значений
	var resultDiv = document.getElementById('result');
	resultDiv.innerHTML = '';
// Задание 2.6 Простые числа в интервале от x1 до x2 Часть 1	
	var primeArr = [];
	function isPrime(x) {
		if(x < 2) {
			return false;
		}
		for (var m = 2; m < x; m++) {
			if(x % m == 0){
				return false;
				break;
			}
		}
					return true;
	}
// Задание 2.2 Проверка на пустой ввод		
	if(document.getElementById('x1').value === '' || document.getElementById('x2').value === ''){
		alert("Поля x1 и x2 должны быть заполнены.");
	}
	else if(Number.isNaN(x1) || Number.isNaN(x2)){
		alert("В поля x1 и x2 должны быть введены числовые значения.");
	} 
// Задание 2.3	Сумма чисел от x1 до x2
	else if(document.getElementById('sum').checked){
		var sum = 0; 
		for(var i = x1; i <= x2; i++){
			sum += i;}
		resultDiv.append("Сумма чисел от x1 до x2 = " + sum);
	}
// Задание 2.5 Произведение чисел от x1 до x2
	else if(document.getElementById('multiply').checked){
		var multiply = 1; 
		for(var i = x1; i <= x2; i++){
			multiply = multiply * i;	
		}
		resultDiv.append("Произведение чисел от x1 до x2 = " + multiply);
	}
// Задание 2.6 Простые числа в интервале от x1 до x2 Часть 2
	else if(document.getElementById('prime').checked){
		for(var i = x1; i <= x2; i++){

			if(isPrime(i)){
				primeArr.push(i);
			}
		}

		resultDiv.append("Простые числа в интервале от x1 до x2 - это " + primeArr);
	}
// Если не выбран ни один из переключателей
		else{
			alert("Должно быть выбрано одно из действий.");
		}
}
// Задание 2.4 Кнопка "Очистить"
function cleanClick(){
	document.getElementById('x1').value = '';
	document.getElementById('x2').value = '';
}
